package io.schiar.bulbasaur.model;

/**
 * Created by User on 16/07/2015.
 */
public enum Status {
    NOTDONE,
    DONE,
    DOING
}
