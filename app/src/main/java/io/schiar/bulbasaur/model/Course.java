package io.schiar.bulbasaur.model;

import java.util.ArrayList;
import java.util.List;

public class Course {
	private String fullName;
	private String name;
	private String code;
	private int phase;
	private Status status;
    private int id;

    public Course() {
        this.setStatus(Status.NOTDONE);
    }

	public Course(String code, String name, int phase) {
		this.name = name;
		this.code = code;
		this.phase = phase;
		this.setStatus(Status.NOTDONE);
	}

    public static List<String> toStringList(List<Course> courses) {
        List<String> strings = new ArrayList<>();
        for (Course course : courses) {
            strings.add(course.toString());
        }
        return strings;
    }

	@Override
	public String toString() {
		return code + "\n" + name;
	}

    public String getFullName() {
        return fullName;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public int getPhase() {
        return phase;
    }

    public Status getStatus() {
		return status;
	}

    public int getId() {
        return id;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setPhase(int phase) {
        this.phase = phase;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void setStatus(String status) {
        this.status  = status.equals("cursada") ? Status.DONE : (status.equals("cursando") ? Status.DOING : Status.NOTDONE);
    }

    public void setId(int id) {
        this.id = id;
    }
}