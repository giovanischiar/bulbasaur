package io.schiar.bulbasaur.model;

import android.graphics.Rect;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by User on 01/08/2015.
 */
public class Curriculum {
    private Map<Course, List<Course>> dependencies;
    private Map<Integer, Course> courseSemester = new HashMap<>();
    private List<Course> courses;
    private String name;
    private Map<Integer, Iterator<String>> semesterIterators;
    private List<Semester> semesters;
    public Rect bounds;

    public Curriculum() {
        dependencies = new HashMap<>();
        semesters = new ArrayList<>();
        name = "";
        courses = new ArrayList<>();
        semesterIterators = new HashMap<>();
    }

    public Map<Course, List<Course>> getDependencies() {
        return dependencies;
    }

    public void setDependencies(Map<Course, List<Course>> dependencies) {
        this.dependencies = dependencies;
    }

    public void addDependency(Dependency dependency) {
        List<Course> tmp = new ArrayList<>();
        tmp.addAll(Arrays.asList(dependency.getDepended()));
        this.dependencies.put(dependency.getDepends(), tmp);
    }

    public List<Integer> phasesOfEachCourse() {
        List<Integer> phases = new ArrayList<>();
        for(Course course : courses) {
            phases.add(course.getPhase());
        }
        return phases;
    }

    public Map<String, Integer> nameAndPhaseOfCourses() {
        Map<String, Integer> map = new HashMap<>();
        for(Course course : courses) {
           map.put(course.toString(), course.getPhase());
        }
        return map;
    }

    public List<String> coursesName() {
        List<String> names = new ArrayList<>();
        for(Course course : courses) {
            names.add(course.toString());
        }
        return names;
    }

    public void setSemesters(List<Semester> semesters) {
        this.semesters = semesters;
    }

    public Map<String, List<String>> coursesToConnectEachOther() {
        Map<String, List<String>> map = new LinkedHashMap<>();
        for (Map.Entry<Course, List<Course>> entry : dependencies.entrySet()) {
            List<String> dependeds = new ArrayList<>();
            for (Course course : entry.getValue()) {
                dependeds.add(course.toString());
            }
            map.put(entry.getKey().toString(), dependeds);
        }
        return map;
    }

    public int howManyCoursesInASemester(int semester) {
        int many = 0;
        for(int i = 0; i < courses.size(); i++) {
            if(courses.get(i).getPhase() == semester) {
                many++;
            }
        }
        return many;
    }

    public Iterator<String> coursesOfSemester(int semesterNum) {
        if (!semesterIterators.containsKey(semesterNum) ||
                !semesterIterators.get(semesterNum).hasNext()) {

            List<String> semesterCourses = new ArrayList<>();
            for(Semester semester : semesters) {
                if (semester.getNum() == semesterNum) {
                    semesterCourses = Course.toStringList(semester.getCourses());
                    break;
                }
            }

            if (semesterCourses.isEmpty()) {
                return semesterCourses.iterator();
            }

            semesterIterators.put(semesterNum, semesterCourses.iterator());
        }

        return semesterIterators.get(semesterNum);
    }

    public boolean isThereCourseIn(int i, int j) {
        boolean aux = false;
        if(i < howManyCoursesInARow(i) && j < howManyCoursesInASemester(j)) {
            aux = true;
        }
        return aux;
    }

    public int howManyCoursesInARow(int row) {
        if (row <= 0 || row >= courses.size()) {return 0;}
        int aux = 0;
        int lastAux = -1;
        for(int i  = 0; i < courses.size(); i++) {
            if(lastAux != courses.get(i).getPhase()) {
                if(i + row -1 < courses.size()) {
                    if(courses.get(i + row -1).getPhase() == courses.get(i).getPhase()) {
                        aux++;
                    }
                }
                lastAux = courses.get(i).getPhase();
            }
        }
        return aux;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }
}