package io.schiar.bulbasaur.model;

/**
 * Created by User on 01/08/2015.
 */
public class Dependency {
    private Course depends;
    private Course[] depended;

    public Dependency() {

    }

    public Dependency(Course depends, Course... depended) {
        this.depends = depends;
        this.depended = depended;
    }

    public Course getDepends() {
        return depends;
    }

    public void setDepends(Course depends) {
        this.depends = depends;
    }

    public Course[] getDepended() {
        return depended;
    }

    public void setDepended(Course[] depended) {
        this.depended = depended;
    }
}
