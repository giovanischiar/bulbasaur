package io.schiar.bulbasaur.model;

import java.util.List;

/**
 * Created by giovani on 26/09/15.
 */
public class Semester {
    private final List<Course> courses;
    private final int num;

    public Semester(int num, List<Course> courses) {
        this.courses = courses;
        this.num = num;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public int getNum() {
        return num;
    }
}
