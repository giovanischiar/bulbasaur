package io.schiar.bulbasaur.controller;

import android.content.Context;
import android.graphics.Rect;

import io.schiar.bulbasaur.model.Curriculum;
import io.schiar.bulbasaur.view.Orderable;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Manager implements Requestable {
    private DataBaseManager dbctrl;
    private WeakReference<Orderable> orderable;
    private Curriculum curriculum;
    private boolean setupComplete;

    public Manager(Context context) {
        curriculum = null;
        setupComplete = false;
        dbctrl = new DataBaseManager(context);
        loadCurriculumFromXML(context, "CCO.xml");
    }

    private void loadCurriculumFromXML(Context context, String xml) {
        AccessorTask task = new AccessorTask(context, dbctrl, this);
        try {
            File xmlFolder = new File(context.getFilesDir().getAbsolutePath() + "/xmls");
            if (!xmlFolder.exists()) {
                xmlFolder.mkdirs();
            }

            String[] assets = context.getAssets().list("");
            String assetXML = assets[0];
            InputStream inputStream = context.getAssets().open(assetXML);
            String xmlPath = xmlFolder.getAbsolutePath() + "/" + assetXML;
            File file = new File(xmlPath);
            FileOutputStream outStream = new FileOutputStream(file);

            StringBuilder builder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));

            String line = reader.readLine();
            while (line != null) {
                builder.append(line);
                line = reader.readLine();
            }

            reader.close();

            outStream.write(builder.toString().getBytes());
            outStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        task.execute(context.getFilesDir().getAbsolutePath() + "/xmls/" + xml);
    }

    public void tryToRun() {
        if(setupComplete) {
            run();
        }
    }

    public void run() {
        Orderable order = orderable.get();
        if (order == null)
            return;
        order.assembleScreen();
        order.connectButtons(curriculum.coursesToConnectEachOther());
        //dbctrl.savedb();
        setupComplete = true;
    }

    public void updateScreen() {
        run();
    }

    public void setOrderable(Orderable orderable) {
        this.orderable = new WeakReference<>(orderable);
    }

    public Map<String, Integer> textAndRowOfAllButtons() {
        return curriculum.nameAndPhaseOfCourses();
    }

    public String requestStatusOf(String name) {
        return dbctrl.getStatusOfSubject(name);
    }

    public void changeStatusOf(String name, String newStatus) {
        dbctrl.changeStatusOfCourse(name, newStatus);
    }

    public List<String> textsToHighlight() {
        return dbctrl.getAvailableSubjects();
    }

    public String currentCurriculumName() {
        return curriculum.getName();
    }

    public Curriculum getCurriculum() {
        return curriculum;
    }

    public void setCurriculum(Curriculum curriculum) {
        this.curriculum = curriculum;
    }

    public Rect getCurriculumBounds() {
        return dbctrl.getCurriculumBounds();
    }

    public int coursesInARow(int row) {
        return curriculum.howManyCoursesInARow(row);
    }

    public Iterator<String> coursesOfSemester(int row) {
        return curriculum.coursesOfSemester(row);
    }

    public int coursesInASemester(int semester) {
        return curriculum.howManyCoursesInASemester(semester);
    }

    public boolean willThereButtonIn(int i, int j) {
        return curriculum.isThereCourseIn(i, j);
    }

}