package io.schiar.bulbasaur.controller;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import io.schiar.bulbasaur.R;
import io.schiar.bulbasaur.model.Curriculum;
import io.schiar.bulbasaur.persistence.XMLParser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Created by User on 02/08/2015.
 */
public class AccessorTask extends AsyncTask<String, Integer, Curriculum> {

    private Context context;
    private Manager manager;
    private DataBaseManager dataBaseManager;

    public AccessorTask(Context context, DataBaseManager dataBaseManager, Manager manager) {
        this.context = context;
        this.dataBaseManager = dataBaseManager;
        this.manager = manager;
    }

    Curriculum xmlToCurriculum(String xmlPath) {
        Curriculum curriculum = null;
        XMLParser xmlParser = new XMLParser();
        try {
            File file = new File(xmlPath);
            curriculum = xmlParser.parse(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return curriculum;
    }

    @Override
    protected Curriculum doInBackground(String... xmlPaths) {
        return curriculumFromXML(xmlPaths[0]);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(Curriculum curriculum) {
        super.onPostExecute(curriculum);
        manager.setCurriculum(curriculum);
        manager.run();
    }

    public Curriculum curriculumFromXML(String xmlPath) {
        Curriculum curriculum = null;
        String preferencesFileName = context.getString(R.string.preferences_file_name);
        SharedPreferences preferences = context.getSharedPreferences(preferencesFileName, Context.MODE_PRIVATE);
        String preferenceKey = context.getString(R.string.current_curriculum);
        String curriculumName = context.getString(R.string.curriculum_name);
        String currentCurriculum = preferences.getString(preferenceKey, null);
        if(currentCurriculum == null) {
            curriculum = xmlToCurriculum(xmlPath);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(preferenceKey, xmlPath);
            editor.putString(curriculumName, curriculum.getName());
            editor.apply();
            dataBaseManager.insertCurriculum(curriculum);
        } else {
            curriculum = dataBaseManager.retrieveCurriculum();
            curriculum.setName(preferences.getString(curriculumName, null));
        }
        return curriculum;
    }
}
