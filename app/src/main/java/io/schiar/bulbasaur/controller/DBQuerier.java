package io.schiar.bulbasaur.controller;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Rect;

import io.schiar.bulbasaur.model.Course;
import io.schiar.bulbasaur.model.Curriculum;
import io.schiar.bulbasaur.model.Semester;
import io.schiar.bulbasaur.persistence.DBCreator;
import io.schiar.bulbasaur.persistence.Queries;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by User on 09/08/2015.
 */
public class DBQuerier {

    private DBCreator database;

    public DBQuerier(DBCreator database) {
        this.database = database;
    }

    public Curriculum retrieveCurriculum() {
        Curriculum curriculum = new Curriculum();
        curriculum.setDependencies(getDependencies());
        curriculum.setCourses(getCourses());
        curriculum.setSemesters(getSemesters());
        curriculum.bounds = getBounds();
        return curriculum;
    }

    private Semester getSemester(int semester) {
        List<Course> coursesInSemester = new ArrayList<>();
        List<Course> courses = getCourses();
        for (int i = 0; i < courses.size(); i++) {
            if (courses.get(i).getPhase() == semester) {
                coursesInSemester.add(courses.get(i));
            }
        }
        return new Semester(semester, coursesInSemester);
    }

    public List<Semester> getSemesters() {
        List<Semester> semesters = new ArrayList<>();
        int width = getBounds().width();
        for(int i = 0; i < width; i++) {
            semesters.add(getSemester(i+1));
        }
        return semesters;
    }

    public String getStatusOfSubject(String courseName) {
        SQLiteDatabase sqLiteDatabase = database.getReadableDatabase();
        String rtrn = "";
        String sql = Queries.STATUS_OF_COURSE.getSql();
        String[] args =  {courseName};
        Cursor cursor;
        cursor = sqLiteDatabase.rawQuery(sql, args);
        if (cursor.moveToNext()) {
            rtrn = cursor.getString(0);
        }
        sqLiteDatabase.close();
        cursor.close();
        return rtrn;
    }

    public void changeStatusOfCourse(String courseName, String newStatus) {
        SQLiteDatabase sqLiteDatabase = database.getWritableDatabase();
        String sql = Queries.UPDATE_STATUS_OF_COURSE.getSql();
        String[] args =  {newStatus, courseName};
        sqLiteDatabase.execSQL(sql, args);
        sqLiteDatabase.close();
    }

    public List<String> getAvailableSubjects() {
        SQLiteDatabase sqLiteDatabase = database.getReadableDatabase();
        List<String> courseNames = new ArrayList<>();
        Cursor cursor;
        String sql = Queries.AVAILABLE_COURSES.getSql();
        cursor = sqLiteDatabase.rawQuery(sql, new String[]{});
        while (cursor.moveToNext()) {
            Course course = new Course(cursor.getString(2), cursor.getString(1), cursor.getInt(3));
            course.setId(cursor.getInt(0));
            course.setStatus(cursor.getString(4));
            courseNames.add(course.toString());
        }
        sqLiteDatabase.close();
        cursor.close();
        return courseNames;
    }

    public List<Course> getCourses() {
        SQLiteDatabase sqLiteDatabase = database.getReadableDatabase();
        List<Course> courses = new ArrayList<>();
        String sql = Queries.ALL_COURSES.getSql();
        Cursor cursor;
        cursor = sqLiteDatabase.rawQuery(sql, new String[]{});
        while (cursor.moveToNext()) {
            Course d = new Course(cursor.getString(2), cursor.getString(1), cursor.getInt(3));
            d.setId(cursor.getInt(0));
            d.setStatus(cursor.getString(4));
            courses.add(d);
        }
        sqLiteDatabase.close();
        cursor.close();
        return courses;
    }

    public List<Course> dependenciesOfCourse(String courseName) {
        SQLiteDatabase sqLiteDatabase = database.getReadableDatabase();
        List<Course> courses = new ArrayList<>();
        Cursor cursor;
        String sql = Queries.DEPENDENCE_OF_COURSE.getSql();
        String[] args =  {courseName};
        cursor = sqLiteDatabase.rawQuery(sql, args);
        while (cursor.moveToNext()) {
            Course d = new Course(cursor.getString(2), cursor.getString(1), cursor.getInt(3));
            d.setId(cursor.getInt(0));
            d.setStatus(cursor.getString(4));
            courses.add(d);
        }
        sqLiteDatabase.close();
        cursor.close();
        return courses;
    }

    public List<Course> derivatesOfCourse(String courseName) {
        SQLiteDatabase sqLiteDatabase = database.getReadableDatabase();
        List<Course> courses = new ArrayList<>();
        String sql = Queries.DERIVATIVES_OF_COURSE.getSql();
        String[] args =  {courseName};
        Cursor result = sqLiteDatabase.rawQuery(sql, args);
        while (result.moveToNext()) {
            Course d = new Course(result.getString(2), result.getString(1), result.getInt(3));
            d.setId(result.getInt(0));
            d.setStatus(result.getString(4));
            courses.add(d);
        }
        sqLiteDatabase.close();
        result.close();
        return courses;
    }

    public Map<Course, List<Course>> getDependencies() {
        Map<Course, List<Course>> map = new LinkedHashMap<>();
        List<Course> courses = getAllDependsSubjects();
        for(Course course : courses) {
            List<Course> dependencies = derivatesOfCourse(course.getName());
            map.put(course, dependencies);
        }
        return map;
    }

    public List<Course> getAllDependsSubjects() {
        SQLiteDatabase sqLiteDatabase = database.getReadableDatabase();
        List<Course> courses = new ArrayList<>();
        String sql = Queries.ALL_DEPENDS_COURSES.getSql();
        Cursor cursor;
        cursor = sqLiteDatabase.rawQuery(sql, new String[]{});
        while (cursor.moveToNext()) {
            Course d = new Course(cursor.getString(2), cursor.getString(1), cursor.getInt(3));
            d.setId(cursor.getInt(0));
            d.setStatus(cursor.getString(4));
            courses.add(d);
        }
        sqLiteDatabase.close();
        cursor.close();
        return courses;
    }

    public Rect getBounds() {
        SQLiteDatabase sqLiteDatabase = database.getReadableDatabase();
        Rect bounds = new Rect(0, 0, 0, 0);
        String sql = Queries.BOUNDS.getSql();
        Cursor cursor;
        cursor = sqLiteDatabase.rawQuery(sql, new String[]{});
        while (cursor.moveToNext()) {
            bounds = new Rect(0, 0, cursor.getInt(0), cursor.getInt(1));
        }
        sqLiteDatabase.close();
        cursor.close();
        return bounds;
    }
}