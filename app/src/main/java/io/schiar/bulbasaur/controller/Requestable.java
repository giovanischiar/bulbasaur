package io.schiar.bulbasaur.controller;

import android.graphics.Rect;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by User on 21/07/2015.
 */
public interface Requestable {
    void updateScreen(); //MainActivity
    String requestStatusOf(String name); //buttonconfig
    void changeStatusOf(String name, String newStatus); //MainActivity
    List<String> textsToHighlight(); //MainActivity
    Map<String, Integer> textAndRowOfAllButtons(); //delete
    Iterator<String> coursesOfSemester(int row); //buttonConfig
    int coursesInASemester(int semester);
    boolean willThereButtonIn(int i, int j);
    Rect getCurriculumBounds(); //Layoutconfig
    int coursesInARow(int row); //LayoutConfig
}