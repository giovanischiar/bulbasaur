package io.schiar.bulbasaur.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Rect;
import android.util.Log;

import io.schiar.bulbasaur.model.Course;
import io.schiar.bulbasaur.model.Curriculum;
import io.schiar.bulbasaur.model.Dependency;
import io.schiar.bulbasaur.model.Status;
import io.schiar.bulbasaur.persistence.DBCreator;
import io.schiar.bulbasaur.persistence.DBSaver;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by User on 16/07/2015.
 */
public class DataBaseManager {

    private DBCreator database;
    private DBQuerier querrier;
    private Context context;

    public DataBaseManager(Context context) {
        this.context = context;
        database = new DBCreator(context);
        querrier = new DBQuerier(database);
    }

    public List<Integer> insertSubject(Course... courses) {
        List<Integer> ids = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = database.getWritableDatabase();
        for (Course course : courses) {
            ContentValues valores;
            valores = new ContentValues();
            valores.put(DBCreator.getNAME(), course.getName());
            valores.put(DBCreator.getCOD(), course.getCode());
            valores.put(DBCreator.getSEMESTER(), course.getPhase());
            boolean isCursada = course.getStatus() == Status.DONE;
            boolean isCursando = course.getStatus() == Status.DOING;
            String status = isCursada ? "cursada" : (isCursando ? "cursando" : "não cursada");
            valores.put(DBCreator.getSTATUS(), status);
            Integer id = (int)sqLiteDatabase.insert(DBCreator.getCOURSES(), null, valores);
            if (id < 0) {
                Log.wtf("Bulbasaur", "Error inserting " + course.getName() + "'s dependencies.");
            }
            ids.add(id);
        }
        sqLiteDatabase.close();
        return ids;
    }

    public void insertCurriculum(Curriculum curriculum) {
        Map<Course, List<Course>> dependencies = curriculum.getDependencies();
        for (Course course : curriculum.getCourses()) {
            if(course != null) {
                int id = insertSubject(course).get(0);
                course.setId(id);
            } else {
                Log.wtf("BDERROR", "course null");
            }
        }
        for (Map.Entry<Course, List<Course>> entry : dependencies.entrySet()) {
            Course depends = entry.getKey();
            List<Course> dependedsList = entry.getValue();
            Course[] dependeds = dependedsList.toArray(new Course[dependedsList.size()]);
            insertDependencies(new Dependency(depends, dependeds));
        }
        curriculum.setSemesters(querrier.getSemesters());
    }

    public Curriculum retrieveCurriculum() {
        return querrier.retrieveCurriculum();
    }

    public void savedb() {
        new DBSaver().copyDBFile(context.getDatabasePath(DBCreator.getNameDatabase()));
    }

    public void insertDependencies(Dependency dependencies) {
        SQLiteDatabase sqLiteDatabase = database.getWritableDatabase();
        Course[] depended = dependencies.getDepended();
        Course depends = dependencies.getDepends();
        for (Course disc : depended) {
            ContentValues valores;
            valores = new ContentValues();
            valores.put(DBCreator.getDEPENDS(), depends.getId());
            valores.put(DBCreator.getDEPENDED(), disc.getId());
            long result = sqLiteDatabase.insert(DBCreator.getDEPENDENCIES(), null, valores);
            if (result < 0) {
                Log.wtf("Bulbasaur", "Error inserting " + depends.getName() + "'s dependencies.");
            }
        }
        sqLiteDatabase.close();
    }

    public String getStatusOfSubject(String courseName) {
        return querrier.getStatusOfSubject(courseName);
    }

    public void changeStatusOfCourse(String courseName, String newStatus) {
        querrier.changeStatusOfCourse(courseName, newStatus);
    }

    public List<String> getAvailableSubjects() {
        return querrier.getAvailableSubjects();
    }

    public List<Course> getSubjects() {
        return querrier.getCourses();
    }

    public List<Course> dependenciesOfCourse(String courseName) {
        return querrier.dependenciesOfCourse(courseName);
    }

    public List<Course> derivatesOfCourse(String courseName) {
        return querrier.derivatesOfCourse(courseName);
    }

    public Rect getCurriculumBounds() {
        return querrier.getBounds();
    }

    public Map<Course, List<Course>> getDependencies() {
        return querrier.getDependencies();
    }

    public List<Course> getAllDependsSubjects() {
        return querrier.getAllDependsSubjects();
    }
}