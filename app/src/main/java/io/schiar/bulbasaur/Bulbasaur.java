package io.schiar.bulbasaur;

import android.app.Application;

import io.schiar.bulbasaur.controller.Manager;
import io.schiar.bulbasaur.view.Communicable;
import io.schiar.bulbasaur.view.Orderable;
import io.schiar.bulbasaur.controller.Requestable;
import io.schiar.bulbasaur.vinewhip.controller.Connectable;
import io.schiar.bulbasaur.vinewhip.controller.Connector;

import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;
import org.acra.sender.HttpSender;

/**
 * Created by User on 22/07/2015.
 */
@ReportsCrashes (
    formUri = "https://giovanischiar.cloudant.com/acra-bulbasaur/_design/acra-storage/_update/report",
    reportType = HttpSender.Type.JSON,
    httpMethod = HttpSender.Method.PUT,
    formUriBasicAuthLogin = "thoutereavercumandrivene",
    formUriBasicAuthPassword = "adc18ca92c7eb08064b3435f3c6d17e096ad0a8a"
)
public class Bulbasaur extends Application {
    private Manager ctrl;
    private Connector connect;

    @Override
    public void onCreate() {
        super.onCreate();
        ACRA.init(this);
    }

    public void subscribeOrderable(Orderable mainActivity) {
        verifyCtrl();
        ctrl.setOrderable(mainActivity);
        ctrl.tryToRun();
    }

    public Requestable getRequester() {
        verifyCtrl();
        return ctrl;
    }

    public Connectable getConnector() {
        verifyVineWhip();
        return connect;
    }

    public void setConnector(Communicable communicable) {
        verifyVineWhip();
        connect.setActivity(communicable);
        connect.setup();
    }

    public void verifyCtrl() {
        if(ctrl == null) {
            ctrl = new Manager(getBaseContext());
        }
    }

    public void verifyVineWhip() {
        if(connect == null) {
            connect = new Connector();
        }
    }
}