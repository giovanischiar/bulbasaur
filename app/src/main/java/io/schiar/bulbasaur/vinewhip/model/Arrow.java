package io.schiar.bulbasaur.vinewhip.model;

import android.graphics.Color;
import android.graphics.PointF;

/**
 * Created by giovani on 08/05/15.
 */
public class Arrow {
    private PointF startPoint;
    private PointF endPoint;
    private PointF auxStartPoint;
    private PointF auxEndPoint;
    private PointF auxMiddlePoint;
    private int color;
    private boolean highLight;

    public Arrow(PointF startPoint, PointF endPoint) {
        this.startPoint = startPoint;
        this.endPoint = endPoint;
        this.color = Color.RED;
        this.highLight = false;
    }

    public Arrow(PointF startPoint, PointF auxStartPoint, PointF auxEndPoint, PointF endPoint) {
        this(startPoint, endPoint);
        this.auxStartPoint = auxStartPoint;
        this.auxEndPoint = auxEndPoint;
        auxMiddlePoint = new PointF(auxEndPoint.x, auxStartPoint.y);
    }

    public void stretch(float value) {
    }

    public PointF getStartPoint() {
        return startPoint;
    }

    public PointF getAuxStartPoint() {
        return auxStartPoint;
    }

    public PointF getMiddlePoint() {
        return auxMiddlePoint;
    }

    public PointF getAuxEndPoint() {
        return auxEndPoint;
    }

    public PointF getEndPoint() {
        return endPoint;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public boolean isHighLight() {
        return highLight;
    }

    public void setHighLight(boolean highLight) {
        this.highLight = highLight;
    }
}
