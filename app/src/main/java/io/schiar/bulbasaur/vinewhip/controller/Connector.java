package io.schiar.bulbasaur.vinewhip.controller;

import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.util.Pair;

import io.schiar.bulbasaur.view.Communicable;
import io.schiar.bulbasaur.vinewhip.model.Arrow;
import io.schiar.bulbasaur.vinewhip.model.Vertex;
import io.schiar.bulbasaur.vinewhip.view.EdgeDrawer;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by giovani on 21/04/15.
 */
public class Connector implements Connectable {
    private EdgeDrawer connector;
    private Communicable communicable;
    private Map<Vertex, List<Vertex>> connections = new LinkedHashMap<>();
    private int stateA, stateB, stateC, stateD;
    private List<Arrow> arrows = new ArrayList<>();
    private Map<Arrow, Pair<Vertex, Vertex>> edges = new LinkedHashMap<>();

    public Connector() {
        clearStates();
    }

    public void setup() {
        this.connector = (EdgeDrawer) communicable.getLayout();
    }

    public void setActivity(Communicable communicable) {
        this.communicable = communicable;
    }

    private void addEdge(Vertex vertex1, Vertex vertex2) {
        if (connections.containsKey(vertex1)) {
            List<Vertex> adjacent = connections.get(vertex1);
            if (!adjacent.contains(vertex2)) {
                adjacent.add(vertex2);
            }
        } else {
            List<Vertex> adjacent = new ArrayList<>();
            adjacent.add(vertex2);
            connections.put(vertex1, adjacent);
        }
    }

    public void connectButtons(Vertex button1, Vertex button2) {
        if (button1 == null || button2 == null) return;
        addEdge(button1, button2);

        //extrair variáveis para o cálculo da posição das setas
        Point p1 = communicable.getGridPosOf(button1);
        Point p2 = communicable.getGridPosOf(button2);
        int rowButton1 = p1.x;
        int columnButton1 = p1.y;
        int rowButton2 = p2.x;
        int columnButton2 = p2.y;

        //position of buttons on grid booleans
        boolean columnNeighbor = columnButton1 + 2 == columnButton2;
        boolean b1UpB2 = rowButton1 < rowButton2;
        boolean b1DownB2 = rowButton1 > rowButton2;
        boolean b1SameRowB2 = rowButton1 == rowButton2;

        //altura e largura dos espacos e botoes
        Rect spaceBounds = communicable.getSpaceBounds();
        int spaceWidth = spaceBounds.width();
        int spaceHeight = spaceBounds.height();

        int button1Width = button1.gimmeWidth();
        int button1Height = button1.gimmeHeight();
        int button2Width = button2.gimmeWidth();
        int button2Height = button2.gimmeHeight();

        //determinar ponto inicial e final de cada seta
        float xb1 = ((button1Width * columnButton1 / 2) + (spaceWidth * columnButton1 / 2) + button1Width) - connector.dimen(io.schiar.bulbasaur.R.dimen.buttonStrokeWidth);
        float yb1 = (button1Height * rowButton1 / 2) + (spaceHeight * rowButton1 / 2) + (button1Height / 2);
        float xb2 = ((button2Width * columnButton2 / 2) + (spaceWidth * columnButton2 / 2)) - connector.dimen(io.schiar.bulbasaur.R.dimen.buttonStrokeWidth);
        float yb2 = (button2Height * rowButton2 / 2) + (spaceHeight * rowButton2 / 2) + (button2Height / 2);

        //tratamento para o caso dos botões em memsa linha e colunas vizinhas
        if (columnNeighbor && b1SameRowB2) {
            Arrow arrow = new Arrow(new PointF(xb1, yb1), new PointF(xb2, yb2));
            arrows.add(arrow);
            edges.put(arrow, new Pair<>(button1, button2));
            return;
        }
        //gerando pontos auxiliares por onde as setas irão passar
        float pontoAX = xb1 + spaceWidth / 2;
        float pontoAY1 = yb1 + button1Height / 2 + spaceHeight / 2;
        float pontoAY2 = yb1 - button1Height / 2 - spaceHeight / 2;

        float pontoAY = pontoAY2;

        float pontoBX = xb2 - spaceWidth / 2;
        float pontoBY1 = yb2 + button2Height / 2 + spaceHeight / 2;
        float pontoBY2 = yb2 - button2Height / 2 - spaceHeight / 2;

        float pontoBY = pontoBY2;

        //na linha superior as setas sempre partem pra linha de espaços de baixo
        if (rowButton1 == 0) {
            pontoAY = pontoAY1;
        }
        if (rowButton2 == 0) {
            pontoBY = pontoBY1;
        }

        //afastar linhas
        //caso onde o botão de origem e destino e estão na mesma linha
        if (rowButton1 == rowButton2) {
            if ((rowButton1 + rowButton2) == 0) {
                stateC = 1;
            }
            switch (stateC) {
                case 0:
                    pontoAY = pontoAY2;
                    pontoBY = pontoBY2;
                    pontoAX -= spaceHeight / 10;
                    pontoBX += spaceHeight / 10;

                    pontoAY -= spaceHeight / 10;
                    pontoBY -= spaceHeight / 10;
                    yb1 -= button1Height / 10;
                    yb2 -= button2Height / 10;
                    stateC = 1;
                    break;
                case 1:
                    pontoAY = pontoAY1;
                    pontoBY = pontoBY1;

                    pontoAX -= spaceHeight / 10;
                    pontoBX += spaceHeight / 10;

                    pontoAY += spaceHeight / 10;
                    pontoBY += spaceHeight / 10;
                    yb1 += button1Height / 10;
                    yb2 += button2Height / 10;
                    stateC = 0;
                    break;
            }
        }
        //linha do botão 2 está acima da linha do botão 1
        else if (rowButton1 > rowButton2) {
            pontoAY = pontoAY2;
            pontoBY = pontoBY1;
            switch (stateA) {
                case 0:
                    pontoAX -= spaceWidth / 5;
                    pontoBX += spaceWidth / 5;

                    pontoAY -= spaceHeight / 5;
                    pontoBY -= spaceHeight / 5;
                    yb1 -= button1Height / 5;
                    yb2 -= button2Height / 5;
                    stateA = 1;
                    break;
                case 1:
                    pontoAX -= (3 * spaceWidth) / 10;
                    pontoBX += (3 * spaceWidth) / 10;

                    pontoAY -= (3 * spaceHeight) / 10;
                    pontoBY -= (3 * spaceHeight) / 10;
                    yb1 -= (3 * button1Height) / 10;
                    yb2 -= (3 * button2Height) / 10;
                    stateA = 0;
                    break;

//                case 2:
//                    pontoAX -= 2 * spaceWidth / 5;
//                    pontoBX += 2 * spaceWidth / 5;
//
//                    pontoAY -= 2 * spaceHeight / 5;
//                    pontoBY -= 2 * spaceHeight / 5;
//                    yb1 -= 2 * button1Height / 5;
//                    yb2 -= 2 * button2Height / 5;
//                    stateA = 0;
//                    break;
            }
        }
        //linha do botao 2 está abaixo da linha do botão 1
        else if (rowButton1 < rowButton2) {
            pontoAY = pontoAY1;
            switch (stateB) {
                case 0:
                    pontoAX -= spaceWidth / 5;
                    pontoBX += spaceWidth / 5;

                    pontoAY += spaceHeight / 5;
                    pontoBY += spaceHeight / 5;
                    yb1 += button1Height / 5;
                    yb2 += button2Height / 5;
                    stateB = 1;
                    break;
                case 1:
                    pontoAX -= (3 * spaceWidth) / 10;
                    pontoBX += (3 * spaceWidth) / 10;

                    pontoAY += (3 * spaceHeight) / 10;
                    pontoBY += (3 * spaceHeight) / 10;
                    yb1 += (3 * button1Height) / 10;
                    yb2 += (3 * button2Height) / 10;
                    stateB = 0;
                    break;
//                case 2:
//                    pontoAX -= 2 * spaceWidth / 5;
//                    pontoBX -= 2 * spaceWidth / 5;
//
//                    pontoAY += 2 * spaceHeight / 5;
//                    pontoBY += 2 * spaceHeight / 5;
//                    yb1 += 2 * button1Height / 5;
//                    yb2 += 2 * button2Height / 5;
//                    stateB = 0;
//                    break;
            }
        }

        //tratamento para os botões que estão em colunas vizinhas, e não em mesma linhas
        if (columnNeighbor && (b1UpB2 || b1DownB2)) {
            //pontoAX = xb1+spaceWidth/2;
            pontoAY = yb1;
            pontoBY = yb2;
        }

        Arrow arrow = new Arrow(new PointF(xb1, yb1), new PointF(pontoAX, pontoAY), new PointF(pontoBX, pontoBY), new PointF(xb2, yb2));
        arrow.setColor(button1.getColorOfArrows());
        arrows.add(arrow);
        edges.put(arrow, new Pair<>(button1, button2));
    }

    public void setAState(Arrow a) {

    }

    public void changeBState() {

    }

    public void changeCState() {

    }

    public void drawArrows() {
        for (Arrow arrow : edges.keySet()) {
            arrow.setColor(edges.get(arrow).first.getColorOfArrows());
        }
        connector.drawConnectors(arrows);
    }

    private void clearStates() {
        stateA = 0;
        stateB = 0;
        stateC = 0;
        stateD = 0;
    }

    public void reConnect(List<Vertex> vertices) {
        arrows.clear();
        clearStates();
        for (Vertex vertex : vertices) {
            List<Vertex> adjacents = connections.get(vertex);
            if (adjacents != null) {
                for (Vertex vertexAdjacent : adjacents) {
                    connectButtons(vertex, vertexAdjacent);
                }
            }
        }
        drawArrows();
    }

    public void setScaleFactor(float scalefactor) {
        connector.updateBounds(scalefactor);
    }
}