package io.schiar.bulbasaur.vinewhip.controller;

import io.schiar.bulbasaur.vinewhip.model.Vertex;

import java.util.List;

/**
 * Created by User on 31/07/2015.
 */
public interface Connectable {
    void connectButtons(Vertex button1, Vertex button2);
    void reConnect(List<Vertex> vertices);
    void setScaleFactor(float scalefactor);
    void drawArrows();
}