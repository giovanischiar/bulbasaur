package io.schiar.bulbasaur.vinewhip.model;

/**
 * Created by User on 16/08/2015.
 */
public interface Vertex {
    int gimmeWidth();
    int gimmeHeight();
    String getText();
    int getColorOfArrows();
}
