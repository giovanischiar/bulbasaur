package io.schiar.bulbasaur.vinewhip.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.widget.RelativeLayout;

import io.schiar.bulbasaur.vinewhip.model.Arrow;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by giovani on 20/04/15.
 */
public class EdgeDrawer extends RelativeLayout {
    List<Arrow> connectors = new ArrayList<Arrow>();
    private Bitmap bitmap;
    private Paint paint;
    private float scaleFactor = 1.0f;
    private static final int BITMAP_WIDTH = 300;
    private static final int BITMAP_HEIGHT = 300;

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Log.d("TIME", System.currentTimeMillis() + " helloooooo :) lets draw");
        //clearCanvas(canvas);
        drawArrows(canvas);
        Log.d("TIME", System.currentTimeMillis() + " done! vlw flws");
    }

    private void clearCanvas(Canvas canvas) {
        int canvasWidth = canvas.getWidth();
        int canvasHeight = canvas.getHeight();
        for (int i = 0; i < canvasWidth; i += BITMAP_WIDTH) {
            for (int j = 0; i < canvasHeight; i += BITMAP_HEIGHT) {
                canvas.drawBitmap(bitmap, i, j, paint);
            }
        }
    }

    private void drawArrows(Canvas canvas) {
        int WIDTHCONNECTOR = (int) (Math.sqrt(scaleFactor) * dimen(io.schiar.bulbasaur.R.dimen.connectorWidth));
        int w = getWidth();
        int h = getHeight();
        Log.d("sss", String.valueOf(w) + " " + String.valueOf(h));
        Log.d("dump", "tamanho de arrows: " + String.valueOf(connectors.size()));
        for (Arrow connector : connectors) {
            Log.d("oie", "from (" + String.valueOf(connector.getStartPoint().x) + ", " + connector.getStartPoint().y + ") to (" + connector.getEndPoint().x + ", " + connector.getEndPoint().y + ")");
            paint.setColor(connector.getColor());
            //paint.setColor(Color.argb(128, Math.abs(gerador.nextInt()) % 255, Math.abs(gerador1.nextInt()) % 255, Math.abs(gerador2.nextInt()) % 255));
            paint.setStrokeWidth(WIDTHCONNECTOR);

            if (connector.getAuxStartPoint() != null) {
                canvas.drawLine(connector.getStartPoint().x, connector.getStartPoint().y, connector.getAuxStartPoint().x, connector.getAuxStartPoint().y, paint);
                if (connector.getMiddlePoint() != null) {
                    canvas.drawLine(connector.getAuxStartPoint().x, connector.getAuxStartPoint().y, connector.getMiddlePoint().x, connector.getMiddlePoint().y, paint);
                    canvas.drawLine(connector.getMiddlePoint().x, connector.getMiddlePoint().y, connector.getAuxEndPoint().x, connector.getAuxEndPoint().y, paint);
                } else {
                    canvas.drawLine(connector.getAuxStartPoint().x, connector.getAuxStartPoint().y, connector.getAuxEndPoint().x, connector.getAuxEndPoint().y, paint);
                }
                canvas.drawLine(connector.getAuxEndPoint().x, connector.getAuxEndPoint().y, connector.getEndPoint().x, connector.getEndPoint().y, paint);
            } else {
                canvas.drawLine(connector.getStartPoint().x, connector.getStartPoint().y, connector.getEndPoint().x, connector.getEndPoint().y, paint);
            }
            drawAArrow(connector, canvas);
        }
    }

    public void drawAArrow(Arrow connector, Canvas canvas) {
        final int SIZEARROW = (int) (Math.sqrt(scaleFactor) * dimen(io.schiar.bulbasaur.R.dimen.sizeArrow));
        final int ARROWRIGHTANGLE = 35;
        final int ARROWLEFTANGLE = -ARROWRIGHTANGLE;

        float tailX = connector.getStartPoint().x;
        float tailY = connector.getStartPoint().y;
        if (connector.getAuxEndPoint() != null) {
            tailX = connector.getAuxEndPoint().x;
            tailY = connector.getAuxEndPoint().y;
        }
        float tipX = connector.getEndPoint().x;
        float tipY = connector.getEndPoint().y;

        float dx = tipX - tailX;
        float dy = tipY - tailY;

        double theta = Math.atan2(dy, dx);

        double rad = Math.toRadians(ARROWRIGHTANGLE); //35 angle, can be adjusted
        double x = tipX - SIZEARROW * Math.cos(theta + rad);
        double y = tipY - SIZEARROW * Math.sin(theta + rad);

        double phi2 = Math.toRadians(ARROWLEFTANGLE);//-35 angle, can be adjusted
        double x2 = tipX - SIZEARROW * Math.cos(theta + phi2);
        double y2 = tipY - SIZEARROW * Math.sin(theta + phi2);

        canvas.drawLine(tipX, tipY, (float) x, (float) y, paint);
        canvas.drawLine(tipX, tipY, (float) x2, (float) y2, paint);

    }

    public void updateBounds(float scaleFactor) {
        this.scaleFactor = scaleFactor;
    }

    public void drawConnectors(List<Arrow> connectors) {
        this.connectors = connectors;
        Log.d("TIME", System.currentTimeMillis() + " invalidating this shit");
        invalidate();
    }

    public int dimen(int name) {
        return (int) getResources().getDimension(name);
    }

    public EdgeDrawer(Context context) {
        super(context);
        init();
    }

    public EdgeDrawer(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EdgeDrawer(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        paint = new Paint();
        //setWillNotDraw(false);
        //bitmap = Bitmap.createBitmap(BITMAP_WIDTH, BITMAP_HEIGHT, Bitmap.Config.ARGB_8888);
        //Canvas canvas = new Canvas(bitmap);
        //canvas.drawColor(Color.RED);
        paint.setColor(Color.RED);
        paint.setStrokeWidth((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, getResources().getDisplayMetrics()));
    }
}