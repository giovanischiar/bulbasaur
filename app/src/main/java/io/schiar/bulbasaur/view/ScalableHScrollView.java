package io.schiar.bulbasaur.view;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.widget.HorizontalScrollView;


/**
 * Created by giovani on 06/09/15.
 */
public class ScalableHScrollView extends HorizontalScrollView implements ScaleFactorRetriever {

    private ScaleGestureDetector mScaleDetector;
    private float scalefactor = 1.0f;
    private Scalable scalable;

    public ScalableHScrollView(Context context) {
        super(context);
        initScaleDetector(context);
    }

    public ScalableHScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initScaleDetector(context);
    }

    public ScalableHScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initScaleDetector(context);
    }

    private void initScaleDetector(Context context) {
        scalefactor = 1;
        mScaleDetector = new ScaleGestureDetector(context, new ScaleGestureDetector.OnScaleGestureListener() {
            @Override
            public void onScaleEnd(ScaleGestureDetector detector) {
                //scalefactor = detector.getScaleFactor();
                //scalable.updateScreen();
                //Log.d("Bulbasaur", "zoom ongoing, scale end: " + detector.getScaleFactor());
            }
            @Override
            public boolean onScaleBegin(ScaleGestureDetector detector) {
                //scalefactor = detector.getScaleFactor();
                //scalable.updateScreen();
                //Log.d("Bulbasaur", "zoom ongoing, scale begin: " + detector.getScaleFactor());
                return true;
            }
            @Override
            public boolean onScale(ScaleGestureDetector detector) {
                Log.d("TIME", System.currentTimeMillis() + " onScale :)");
                scalefactor *= detector.getScaleFactor();
                Log.d("dump", "scale factor: " + String.valueOf(scalefactor) + " original: " + String.valueOf(detector.getScaleFactor()));
                Log.d("TIME", System.currentTimeMillis() + " got scale factor");
                Looper mainLooper = Looper.getMainLooper();
                Handler handler = new Handler(mainLooper);
                handler.post(new Runnable() {
                        @Override
                        public void run() {
                            scalable.updateScreen(scalefactor);
                        }
                    });
                Log.d("TIME", System.currentTimeMillis() + " updated screen, lets return");
                //Log.d("Bulbasaur", "zoom ongoing, scale: " + detector.getScaleFactor());
                return false;
            }
        });
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        super.dispatchTouchEvent(ev);
        return mScaleDetector.onTouchEvent(ev);
    }

    @Override
    public float getScalefactor() {
        return scalefactor;
    }

    public void registerScalable(Scalable scalable) {
    	this.scalable = scalable;
    } 
}
