package io.schiar.bulbasaur.view;

import android.os.Build;
import android.support.v7.widget.Toolbar;
import android.view.ViewConfiguration;

/**
 * Created by Giovani on 23/11/2015.
 */
public class ToolbarBucket {
    private Toolbar appBar;
    public ToolbarBucket(Toolbar appBar) {
        this.appBar = appBar;
        resolveOverflowButton();
    }

    private void resolveOverflowButton() {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1 ||
                (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH &&
                        ViewConfiguration.get(appBar.getContext()).hasPermanentMenuKey())) {
            if (appBar.getOverflowIcon() != null) {
                appBar.getOverflowIcon().setAlpha(0);
            }
        }
    }

}
