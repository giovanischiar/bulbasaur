package io.schiar.bulbasaur.view;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import io.schiar.bulbasaur.Bulbasaur;
import io.schiar.bulbasaur.controller.Requestable;
import io.schiar.bulbasaur.vinewhip.controller.Connectable;
import io.schiar.bulbasaur.vinewhip.model.Vertex;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements Orderable, Scalable, Notifiable {
    private Connectable connector;
    private Requestable requester;
    private Button currentButton;
    private LayoutConfig layoutConfig;
    private ScalableHScrollView scrollView;
    private StatusBarBucket statusBar;
    private Map<String, Integer> statusToColor;
    private FabAvaliableBucket fabAvaliable;
    private FabMarkCoursesBucket fabMarkCourses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(io.schiar.bulbasaur.R.layout.activity_main);
        ScreenMeasures.getInstance().setContext(this);
        setSupportActionBar((Toolbar) findViewById(io.schiar.bulbasaur.R.id.app_bar));
        Bulbasaur app = ((Bulbasaur) getApplication());
        connector = app.getConnector();
        requester = app.getRequester();
        scrollView = (ScalableHScrollView) findViewById(io.schiar.bulbasaur.R.id.hscrollview);
        scrollView.registerScalable(this);
        layoutConfig = new LayoutConfig(requester, (RelativeLayout) findViewById(io.schiar.bulbasaur.R.id.GridLayout1), this);
        app.setConnector(layoutConfig);
        app.subscribeOrderable(this);
        initializeStatusToColor();
        initBuckets();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateScreen(scrollView.getScalefactor());
    }

    private void initBuckets() {
        statusBar = new StatusBarBucket((TextView)findViewById(io.schiar.bulbasaur.R.id.SelectionBar));
        fabAvaliable = new FabAvaliableBucket(findViewById(io.schiar.bulbasaur.R.id.fabAvaliableCourses), this);
        fabMarkCourses = new FabMarkCoursesBucket( new ArrayList<FloatingActionButton>() {{
            add((FloatingActionButton)findViewById(io.schiar.bulbasaur.R.id.fabUnmark));
            add((FloatingActionButton)findViewById(io.schiar.bulbasaur.R.id.fabCoursing));
            add((FloatingActionButton)findViewById(io.schiar.bulbasaur.R.id.fabCoursed));
        }}, this);
        new ToolbarBucket((Toolbar) findViewById(io.schiar.bulbasaur.R.id.app_bar));
    }

    @SuppressWarnings("deprecation")
    private void initializeStatusToColor() {
        statusToColor = new HashMap<String, Integer>(){{
            put("Cursada", getBaseContext().getResources().getColor(io.schiar.bulbasaur.R.color.coursedColorButton));
            put("Cursando", getBaseContext().getResources().getColor(io.schiar.bulbasaur.R.color.coursingColorButton));
            put("Não cursada", getBaseContext().getResources().getColor(io.schiar.bulbasaur.R.color.backgroundColor));}
        };
    }

    @Override
    public void updateStatusBarAtt(int visibility, CharSequence text) {
        statusBar.setContent(text);
        statusBar.setVisibility(visibility);
        if(visibility == View.VISIBLE) {
            fabAvaliable.vanish();
        } else {
            fabMarkCourses.vanish();
        }
    }

    public void unkMarkButtonsIsGone() {
        fabAvaliable.reappear();
    }

    public void fabAvailbleIsGone() {
        fabMarkCourses.appear();
    }

    public void assembleScreen() {
        layoutConfig.fillButtonsOnGrid();
        //setContextMenu(layoutConfig.getButtons());
    }

    public void setContextMenu(List<Vertex> buttons) {
        for(Vertex button : buttons) {
            registerForContextMenu((Button) button);
        }
    }

    public void redrawArrows() {
        connector.drawArrows();
    }

    public void connectButtons(Map<String, List<String>> buttonTextToConnect) {
        findViewById(io.schiar.bulbasaur.R.id.ProgressBar).setVisibility(View.GONE);
        for(int i = 0; i < buttonTextToConnect.keySet().size(); i++) {
            String buttonText = (String)buttonTextToConnect.keySet().toArray()[i];
            Button button = layoutConfig.buttonOf(buttonText);
            for(String adjacentTextButton : buttonTextToConnect.get(buttonText)) {
                Button adjacentButton = layoutConfig.buttonOf(adjacentTextButton);
                connector.connectButtons(button, adjacentButton);
            }
        }
//        Button b1 = layoutConfig.buttonOf("INE5403\nFMD");
//        Button b21 = layoutConfig.buttonOf("INE5411\nOC I");
//        Button b22 = layoutConfig.buttonOf("INE5410\nProg conc");
//        Button b23 = layoutConfig.buttonOf("INE5409\nCalc N CCO");
//        Button b24 = layoutConfig.buttonOf("MTM5245\nAL");
//        Button b25 = layoutConfig.buttonOf("INE5408\nED");
//        Button b26 = layoutConfig.buttonOf("MTMXX\nCalc B CCO");
//        Button b27 = layoutConfig.buttonOf("INE5413\nGrafos");
//        Button b28 = layoutConfig.buttonOf("INE5412\nSO I");
//        Button b29 = layoutConfig.buttonOf("INE5414\nRedes I");
//        Button b210 = layoutConfig.buttonOf("INE5415\nTC");
//        Button b211 = layoutConfig.buttonOf("INE5416\nParadigmas");
//
//        connector.connectButtons(b1, b21);
//        connector.connectButtons(b1, b22);
//        connector.connectButtons(b1, b25);
//        connector.connectButtons(b1, b23);
//        connector.connectButtons(b1, b24);
//        connector.connectButtons(b1, b26);
//        connector.connectButtons(b1, b27);
//        connector.connectButtons(b1, b28);
//        connector.connectButtons(b1, b29);
//        connector.connectButtons(b1, b210);
//        connector.connectButtons(b1, b211);
        connector.drawArrows();
    }

    @SuppressWarnings("deprecation")
    public void textsToHighlight(int color) {
        List<String> buttonTexts = requester.textsToHighlight();
        for(String buttonText : buttonTexts) {
            Button button = layoutConfig.buttonOf(buttonText);
            assert button != null;
            layoutConfig.colorButton(button, getBaseContext().getResources().getColor(color));
        }
    }

//    @Override
//    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
//        super.onCreateContextMenu(menu, v, menuInfo);
//        menu.setHeaderTitle(((TextView) v.findViewById(R.id.name)).getText());
//        menu.add(0, v.getId(), 0, R.string.action_changeToDone);
//        menu.add(0, v.getId(), 0, R.string.action_changeToDoing);
//        menu.add(0, v.getId(), 0, R.string.action_changeToNotDone);
//        currentButton = (Button)v;
//    }
//
//    @Override
//    public boolean onContextItemSelected(MenuItem item) {
//        String stateName = (String)item.getTitle();
//        requester.changeStatusOf(currentButton.getSecondFieldText(), stateName.toLowerCase());
//        layoutConfig.colorButton(currentButton, statusToColor.get(stateName));
//        return true;
//    }

    public void changeStatusOfCourses(String status) {
        for(Button button : layoutConfig.getButtonInFocus()) {
            requester.changeStatusOf(button.getSecondFieldText(), status.toLowerCase());
            layoutConfig.colorButton(button, statusToColor.get(status));
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            return true;
//        } else if(id == R.id.action_highlightSomeButtons) {
//            textsToHighlight(R.color.highlightAvailableButton);
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        updateScreen(scrollView.getScalefactor());
    }

    public void updateScreen(float scaleFactor) {
        if (scaleFactor == 1.0f) return;
        layoutConfig.update(scaleFactor);
        connector.setScaleFactor(scaleFactor);
        connector.reConnect(layoutConfig.getButtons());
    }
}