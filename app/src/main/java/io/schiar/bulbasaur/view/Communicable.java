package io.schiar.bulbasaur.view;

import android.graphics.Point;
import android.graphics.Rect;
import android.view.View;

import io.schiar.bulbasaur.vinewhip.model.Vertex;

/**
 * Created by Giovani on 31/07/2015.
 */
public interface Communicable {
    View getLayout();
    Point getGridPosOf(Vertex button);
    Rect getSpaceBounds();
}
