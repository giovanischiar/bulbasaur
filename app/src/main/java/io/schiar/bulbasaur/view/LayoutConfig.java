package io.schiar.bulbasaur.view;

import android.graphics.Point;
import android.graphics.Rect;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import io.schiar.bulbasaur.R;
import io.schiar.bulbasaur.controller.Requestable;
import io.schiar.bulbasaur.vinewhip.model.Vertex;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LayoutConfig implements Communicable {
    private Map<Vertex, Point> buttonsOnGrid = new HashMap<>();
    private RelativeLayout layout;
    private LayoutInflater inflater;
    private Requestable requester;
    private int semesterColumn = 0, semesterRow = 1;
    private ButtonConfig buttonConfig;
    private float currentScaleFactor = 1.0f;
    private LayoutEventHandler layoutEventHandler;

    public LayoutConfig(Requestable requester, RelativeLayout layout, Notifiable notifier) {
        this.layout = layout;
        this.requester = requester;
        buttonConfig = new ButtonConfig(requester);
        layoutEventHandler = new LayoutEventHandler(notifier, this);
        inflater = LayoutInflater.from(layout.getContext());
        setClickListener();
        layout.removeAllViews();
    }

    private void setClickListener() {
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutEventHandler.clearFocusButton();
            }
        });
    }

    public void fillButtonsOnGrid() {
        boolean addedButtonToThisRow = true;
        Rect bounds = requester.getCurriculumBounds();
        int columnCount = (bounds.width()* 2)-1;
        int rowCount = (bounds.height()*2)-1;
        for(int i = 0; i < rowCount; i++) {
            semesterColumn = 0;
            int sizeRow = requester.coursesInARow(semesterRow);
            for(int j = 0; j < columnCount; j++) {
                if(i%2 == 0 && j%2 == 0) {
                    int sizeColumn = requester.coursesInASemester(semesterColumn + 1);
                    if (semesterColumn+1 <= sizeRow*2-1 && semesterRow <= sizeColumn) {
                        putButton(new Point(i, j));
                    }
                    semesterColumn++;
                    addedButtonToThisRow = true;
                } else {
                    addedButtonToThisRow = false;
                }
            }
            if(addedButtonToThisRow) {
                semesterRow++;
            }
        }
    }

    private void putButton(Point point) {
        Button button = (Button) inflater.inflate(R.layout.buttonlayout, null, false);
        button.setLayoutEventHandler(layoutEventHandler);
        LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        lp.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        setPosition(button, point, lp);
        buttonConfig.configure(button, semesterColumn + 1);
        layout.addView(button);
        buttonsOnGrid.put(button, point);
    }

    private void setPosition(Button button, Point point, LayoutParams lp) {
        int buttonWDimen = (int) (dimen(R.dimen.buttonWidth)/2.0f);
        int buttonHDimen = (int) (dimen(R.dimen.buttonHeight)/2.0f);
        int spaceWDimen = (int) (dimen(R.dimen.spaceWidth)/2.0f);
        int spaceHDimen = (int)(dimen(R.dimen.spaceHeight)/2.0f);
        lp.leftMargin = point.y * (buttonWDimen + spaceWDimen);
        lp.topMargin =  point.x * (buttonHDimen + spaceHDimen);
        button.setLayoutParams(lp);
    }

    public void colorButton(Button button, int color) {
        buttonConfig.colorButton(button, color);
    }

    private int dimen(int name) {
        return (int) (currentScaleFactor * layout.getContext().getResources().getDimension(name));
    }

    public List<Vertex> getButtons() {
        return new ArrayList<>(buttonsOnGrid.keySet());
    }

    public Button buttonOf(String text) {
        return buttonConfig.buttonOf(text);
    }

    public Rect getSpaceBounds() {
        int width = dimen(R.dimen.spaceWidth);
        int height = dimen(R.dimen.spaceHeight);
        return new Rect(0, 0, width, height);
    }

    public Point getGridPosOf(Vertex button) {
        return buttonsOnGrid.get(button);
    }

    public View getLayout() {
        return layout;
    }

    public void update(float scaleFactor) {
        currentScaleFactor = scaleFactor;
        for(Vertex v : buttonsOnGrid.keySet()) {
            Button button = (Button) v;
            LayoutParams lp = (LayoutParams) button.getLayoutParams();
            Point point = buttonsOnGrid.get(button);
            setPosition(button, point, lp);
            buttonConfig.updateButton(button, currentScaleFactor);
        }
    }

    public List<Button> getButtonInFocus() {
        return layoutEventHandler.getButtonInFocus();
    }
}