package io.schiar.bulbasaur.view;

import android.view.View;

import io.schiar.bulbasaur.vinewhip.model.Vertex;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Giovani on 13/12/2015.
 */
public class LayoutEventHandler {
    private List<Button> buttonInFocus = new ArrayList<>();
    private LayoutConfig layoutConfig;
    private Notifiable notifier;

    public LayoutEventHandler(Notifiable notifiable, LayoutConfig layoutConfig) {
        this.notifier = notifiable;
        this.layoutConfig = layoutConfig;
    }

    public void switchClickButtons(boolean enable) {
        for (Vertex b : layoutConfig.getButtons()) {
            ((View)b).setClickable(enable);
        }
    }

    public void clearFocusButton() {
        if(buttonInFocus.isEmpty()) {
            return;
        }
        buttonInFocus.clear();
        resolveStatusBarState();
        updateStateOfButtons();
        notifier.redrawArrows();
    }

    public void buttonClicked(Button button) {
        analyzeButtonFocusList(button);
        resolveStatusBarState();
        updateStateOfButtons();
        notifier.redrawArrows();
    }

    private void analyzeButtonFocusList(Button button) {
        if (!buttonInFocus.contains(button)) {
            buttonInFocus.add(button);
        } else {
            buttonInFocus.remove(button);
        }
    }

    private void resolveStatusBarState() {
        if(buttonInFocus.isEmpty()) {
            notifier.updateStatusBarAtt(View.GONE, null);
            return;
        }

        CharSequence contentOnUpdating;
        if (buttonInFocus.size() == 1) {
            CharSequence button2ndField = buttonInFocus.get(0).getSecondFieldText();
            contentOnUpdating = "\"" + button2ndField + "\" selecionado";
        } else {
            CharSequence buttonsInfocusQtd = String.valueOf(buttonInFocus.size());
            contentOnUpdating = buttonsInfocusQtd + " cursos selecionados";
        }
        notifier.updateStatusBarAtt(View.VISIBLE, contentOnUpdating);
    }

    private void updateStateOfButtons() {
        for(Vertex vertex : layoutConfig.getButtons()) {
            Button button = (Button) vertex;
            if(buttonInFocus.contains(button) || buttonInFocus.isEmpty()) {
                button.activate();
            } else {
                button.deactivate();
            }
        }
    }

    public List<Button> getButtonInFocus() {
        return buttonInFocus;
    }
}
