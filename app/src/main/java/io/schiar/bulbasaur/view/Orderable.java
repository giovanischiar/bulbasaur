package io.schiar.bulbasaur.view;

import java.util.List;
import java.util.Map;

/**
 * Created by User on 21/07/2015.
 */
public interface Orderable {
    void connectButtons(Map<String, List<String>> dependencies);
    void assembleScreen();
}