package io.schiar.bulbasaur.view;

import android.graphics.drawable.Drawable;
import android.support.design.widget.FloatingActionButton;
import android.view.View;

import io.schiar.bulbasaur.R;

/**
 * Created by Giovani on 07/12/2015.
 */
public class FabAvaliableBucket extends FabBucket {
    private ScreenMeasures sm = ScreenMeasures.getInstance();
    private FloatingActionButton fab;
    private Notifiable notifiable;
    private boolean isAvailableCoursesShowed;
    private boolean isVanished;

    public FabAvaliableBucket(View fab, Notifiable notifiable) {
        this.fab = (FloatingActionButton)fab;
        this.notifiable = notifiable;
        isAvailableCoursesShowed = false;
        isVanished = false;
        setClickListener();
    }

    public void vanish() {
        if(!isVanished) {
            fab.startAnimation(translate(fab, fab.getX() + sm.dp(100), Direction.RIGHT));
            isVanished = true;
        }
    }

    public void reappear() {
        isVanished = false;
        fab.startAnimation(translate(fab, fab.getX() + sm.dp(100), Direction.LEFT));
    }

    @Override
    public void onAppearStart() {
        fab.setVisibility(View.VISIBLE);
    }

    @Override
    public void onAppearEnd() {
        fab.clearAnimation();
    }

    @Override
    public void onVanishStart() {
        notifiable.fabAvailbleIsGone();
    }

    @Override
    public void onVanishEnd() {
        fab.clearAnimation();
        fab.setVisibility(View.GONE);
    }

    private void setClickListener() {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAvailableCourses();
            }
        });
    }

    private void showAvailableCourses() {
        if(!isAvailableCoursesShowed) {
            changeIcon(fab, fab.getResources().getDrawable(R.drawable.ic_highlight_off_white_24dp));
            notifiable.textsToHighlight(R.color.highlightAvailableButton);
            isAvailableCoursesShowed = true;
        } else {
            changeIcon(fab, fab.getResources().getDrawable(R.drawable.ic_lock_open_white_24dp));
            notifiable.textsToHighlight(R.color.backgroundColor);
            isAvailableCoursesShowed = false;
        }
    }

    private void changeIcon(FloatingActionButton fab, Drawable drawable) {
        fab.setImageDrawable(drawable);
    }
}
