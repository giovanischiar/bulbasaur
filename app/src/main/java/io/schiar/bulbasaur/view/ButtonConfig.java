package io.schiar.bulbasaur.view;

import android.util.Log;
import android.widget.TextView;

import io.schiar.bulbasaur.R;
import io.schiar.bulbasaur.controller.Requestable;

import java.util.HashMap;
import java.util.Map;

public class ButtonConfig {
    private Map<String, Button> buttonText = new HashMap<>();
    private Requestable requester;
    private Button currentButton;
    private String currentText;
    private int count = 0;
    private int[] colors;

    public ButtonConfig(Requestable requester)  {
        this.requester = requester;
	}

    public void configure(Button button, int buttonColumn) {
        this.currentButton = button;
        setColors();
        this.currentText = requester.coursesOfSemester(buttonColumn).next();
        setColorOfArrows();
        defineTextFields();
        defineShape();
        defineSize();
    }

    @SuppressWarnings("deprecation")
    private void setColorOfArrows() {
       currentButton.setColorOfArrows(colors[count++ % colors.length]);
       currentButton.setBackgroundDrawable(currentButton.getContext().getResources().getDrawable(R.drawable.buttonshape));
    }

    private void defineSize() {
        currentButton.setDefaultSize();
    }

   private void defineTextFields() {
       //preencher nome da disciplina e o código
       TextView firstTextField = (TextView)currentButton.findViewById(R.id.name);
       TextView secondTextField = (TextView)currentButton.findViewById(R.id.code);
       String[] fields = currentText.split("\n");
       firstTextField.setText(fields[0]);
       secondTextField.setText(fields[1]);
       currentButton.setText(currentText);
       buttonText.put(currentText, currentButton);
   }

    @SuppressWarnings("deprecation")
   private void defineShape() {
       //definir a cor do botão através do status da disciplina que ele representa
        currentButton.setBackgroundDrawable(currentButton.getContext().getResources().getDrawable(R.drawable.buttonshape));
        String status = requester.requestStatusOf(rawName(currentText));
        switch(status) {
            case "cursada":
                colorButton(currentButton, color(R.color.coursedColorButton));
                currentButton.setColor(color(R.color.coursedColorButton));
                break;
            case "cursando":
                colorButton(currentButton, color(R.color.coursingColorButton));
                currentButton.setColor(color(R.color.coursingColorButton));
                break;
            default:
                colorButton(currentButton, color(R.color.buttonColorDefault));
                currentButton.setColor(color(R.color.buttonColorDefault));
        }
   }

    public Button buttonOf(String text) {
       Button buttonToReturn = buttonText.get(text);
       if(buttonToReturn == null)  {
       		Log.wtf("ACTIVITYERROR", "button given text not found!");
       }
       return buttonToReturn;
   }

   private void setColors() {
       colors = new int[] {color(R.color.acropoflies), color(R.color.bee), color(R.color.bluejellyfish), color(R.color.antihero),
               color(R.color.color1), color(R.color.color2), color(R.color.color3), color(R.color.conradgold),
               color(R.color.greentea), color(R.color.tuesday), color(R.color.cutered), color(R.color.greentea),
               color(R.color.jellyfish), color(R.color.lavendar), color(R.color.ryouparentsalive), color(R.color.LIME),
               color(R.color.margarita), color(R.color.suckedmeintosky), color(R.color.mustardo), color(R.color.RaspberryRipples),
               color(R.color.cutered), color(R.color.quoalia), color(R.color.SpaceCops)};
   }

    private String rawName(String textOfBothFields) {
        return textOfBothFields.substring(textOfBothFields.indexOf("\n") + 1, textOfBothFields.length());
    }

    @SuppressWarnings("deprecation")
    public int color(int name) {
        return currentButton.getContext().getResources().getColor(name);
    }

    public void updateButton(Button button, float scaleFactor) {
        button.scale(scaleFactor);
    }

    public void colorButton(Button button, int color) {
        button.setColor(color);
    }
}