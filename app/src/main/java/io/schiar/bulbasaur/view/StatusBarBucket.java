package io.schiar.bulbasaur.view;

import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by Giovani on 22/11/2015.
 */
public class StatusBarBucket {
    public enum State {UNDER, OVER}
    TextView selectionBar;
    CharSequence content;
    State state;
    public StatusBarBucket(TextView statusBar) {
        selectionBar = statusBar;
        state = State.UNDER;
        content = null;
    }

    public void setContent(CharSequence content) {
        this.content = content;
    }

    public void setVisibility(int visibility) {
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) selectionBar.getLayoutParams();
        if(content != null) selectionBar.setText(content);
        if(state == State.OVER) {
            lp.bottomMargin = -lp.height;
            selectionBar.setLayoutParams(lp);
        }
        if(visibility == View.GONE) {
            slideToBottom(selectionBar);
            lp.bottomMargin = -lp.height;
            selectionBar.setLayoutParams(lp);
            state = State.UNDER;
        } else if(state == State.UNDER){
            slideToTop(selectionBar);
            state = State.OVER;
        }
    }

    private void slideToBottom(View view){
        TranslateAnimation animate = new TranslateAnimation(0,0,-view.getHeight(),0);
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    private void slideToTop(View view){
        TranslateAnimation animate = new TranslateAnimation(0,0,0,-view.getHeight());
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }
}
