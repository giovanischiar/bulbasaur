package io.schiar.bulbasaur.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import io.schiar.bulbasaur.R;
import io.schiar.bulbasaur.vinewhip.model.Vertex;

/**
 * Created by User on 16/08/2015.
 */
public class Button extends RelativeLayout implements Vertex, View.OnClickListener, View.OnCreateContextMenuListener {
    public enum State {STANDING, HIGHLIGHTED, ACTIVED, DEACTIVATED}
    private ScreenMeasures sm = ScreenMeasures.getInstance();
    private int colorOfArrows;
    private String text;
    private State state;
    private int color = Color.WHITE;
    private LayoutEventHandler layoutEventHandler;

    public Button(Context context) {
        super(context);
        init();
    }

    public Button(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public Button(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public void init() {
        text = "";
        state = State.STANDING;
        setOnClickListener(this);
        setOnCreateContextMenuListener(this);
    }

    public void setLayoutEventHandler(LayoutEventHandler layoutEventHandler) {
        this.layoutEventHandler = layoutEventHandler;
    }

    @Override
    public void onClick(View v) {
        layoutEventHandler.buttonClicked(this);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        ((Activity)getContext()).onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle(((TextView) v.findViewById(R.id.name)).getText());
        menu.add(0, v.getId(), 0, R.string.action_changeToDone);
        menu.add(0, v.getId(), 0, R.string.action_changeToDoing);
        menu.add(0, v.getId(), 0, R.string.action_changeToNotDone);
    }

    public void activate() {
        state = State.ACTIVED;
        update();
    }

    public void deactivate() {
        state = State.DEACTIVATED;
        update();
    }

    public void setDefaultSize() {
        LayoutParams lp = (LayoutParams) getLayoutParams();
        lp.height = sm.dimen(R.dimen.buttonHeight);
        lp.width = sm.dimen(R.dimen.buttonWidth);
        setLayoutParams(lp);
    }

    public void scale(float scaleFactor) {
        LayoutParams lp = (LayoutParams) getLayoutParams();
        lp.height = (int) (scaleFactor * sm.dimen(R.dimen.buttonHeight));
        lp.width = (int) (scaleFactor * sm.dimen(R.dimen.buttonWidth));
        setLayoutParams(lp);
        scaleText(scaleFactor);
    }

    private void scaleText(float scaleFactor) {
        TextView firstTextField = (TextView)findViewById(R.id.name);
        TextView secondTextField = (TextView)findViewById(R.id.code);
        firstTextField.setTextSize(scaleFactor / (float) sm.sp(1) * getResources().getDimension((R.dimen.sizeOfTextButton)));
        secondTextField.setTextSize(scaleFactor / (float) sm.sp(1) * getResources().getDimension(R.dimen.sizeOfTextButton));
    }

    private void update() {
        GradientDrawable drawable = (GradientDrawable) getBackground();
        switch(state) {
            case STANDING:
            case ACTIVED:
                drawable.setStroke((int) sm.dimen(R.dimen.buttonStrokeWidth), sm.changeColorOpacity(sm.color(R.color.strokeColor), 255));
                drawable.setColor(sm.changeColorOpacity(getColor(), 255));
                ((TextView) findViewById(R.id.name)).setTextColor(sm.changeColorOpacity(sm.color(R.color.textButtonColor), 255));
                ((TextView) findViewById(R.id.code)).setTextColor(sm.changeColorOpacity(sm.color(R.color.textButtonColor), 255));
                changeOpacityOfArrow(255);
                break;
            case DEACTIVATED:
                drawable.setStroke(sm.dimen(R.dimen.buttonStrokeWidth), sm.changeColorOpacity(sm.color(R.color.strokeColor), 42));
                drawable.setColor(sm.changeColorOpacity(getColor(), 42));
                ((TextView) findViewById(R.id.name)).setTextColor(sm.changeColorOpacity(sm.color(R.color.textButtonColor), 42));
                ((TextView) findViewById(R.id.code)).setTextColor(sm.changeColorOpacity(sm.color(R.color.textButtonColor), 42));
                changeOpacityOfArrow(42);
                break;
        }
        ((MainActivity)getContext()).redrawArrows();
    }

    private void changeOpacityOfArrow(int opacity) {
        int red = Color.red(colorOfArrows);
        int green = Color.green(colorOfArrows);
        int blue = Color.blue(colorOfArrows);
        colorOfArrows = Color.argb(opacity, red, green, blue);
    }

    public int gimmeWidth() {
        return getLayoutParams().width;
    }

    public int gimmeHeight() {
        return getLayoutParams().height;
    }

    public int getColorOfArrows() {
        return colorOfArrows;
    }

    public void setColorOfArrows(int color) {
        colorOfArrows = color;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        GradientDrawable drawable = (GradientDrawable) getBackground();
        this.color = color;
        drawable.setColor(color);

    }

    public String getSecondFieldText() {
        return text.substring(text.indexOf("\n")+1, text.length());
    }
}
