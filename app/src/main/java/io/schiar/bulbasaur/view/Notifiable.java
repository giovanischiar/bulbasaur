package io.schiar.bulbasaur.view;

/**
 * Created by giovani on 11/10/15.
 */
public interface Notifiable {
    void redrawArrows();
    void updateStatusBarAtt(int visibility, CharSequence text);
    void textsToHighlight(int color);
    void changeStatusOfCourses(String status);
    void unkMarkButtonsIsGone();
    void fabAvailbleIsGone();
}
