package io.schiar.bulbasaur.view;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;

/**
 * Created by Giovani on 13/12/2015.
 */
public abstract class FabBucket {
    protected int DEFAULT_DURATION_ANIMATION = 500;
    private Collapsable collapsable;
    public abstract void onAppearStart();
    public abstract void onAppearEnd();
    public abstract void onVanishStart();
    public abstract void onVanishEnd();

    public void setCollapsable(Collapsable collapsable) {
        this.collapsable = collapsable;
    }

    protected TranslateAnimation translate(View view, float offset, final Direction direction) {
        final TranslateAnimation animate;
        switch (direction) {
            case RIGHT:
                animate = new TranslateAnimation(0, offset, 0, 0);
                break;
            case LEFT:
                animate = new TranslateAnimation(offset, 0, 0, 0);
                break;
            case UP:
                animate = new TranslateAnimation(0, 0, 0, -offset);
                break;
            case DOWN:
                animate = new TranslateAnimation(0, 0, 0, offset);
                break;
            default:
                animate = new TranslateAnimation(offset, 0, 0, 0);
        }
        animate.setDuration(DEFAULT_DURATION_ANIMATION);
        animate.setFillAfter(false);
        animate.setAnimationListener(new AnimListener(view, offset, direction));
        return animate;
    }

    private class AnimListener implements Animation.AnimationListener {
        private Direction direction;
        private float offset;
        private View view;
        public AnimListener(View view, float offset, Direction direction) {
            this.offset = offset;
            this.direction = direction;
            this.view = view;
        }

        @Override
        public void onAnimationStart(Animation animation) {
            resolveAnimationStart();
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            resolveAnimationEnd();
        }

        private void resolveAnimationStart() {
            switch(direction) {
                case RIGHT:
                    onVanishStart();
                    break;
                case LEFT:
                    onAppearStart();
                    break;
                case UP:
                    collapsable.onExpandStart(view, offset);
                    break;
                case DOWN:
                    collapsable.onCollapseStart(view);
            }
        }

        private void resolveAnimationEnd() {
            switch (direction) {
                case RIGHT:
                    onVanishEnd();
                    break;
                case LEFT:
                    onAppearEnd();
                    break;
                case UP:
                    collapsable.onExpandEnd(view, offset);
                    break;
                case DOWN:
                    collapsable.onCollapseEnd(view);
            }
        }
    }

    enum Direction {
        RIGHT,
        LEFT,
        UP,
        DOWN
    }
}
