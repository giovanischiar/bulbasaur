package io.schiar.bulbasaur.view;

import android.support.design.widget.FloatingActionButton;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Giovani on 07/12/2015.
 */

public class FabMarkCoursesBucket extends FabBucket implements Collapsable {
    private ScreenMeasures sm = ScreenMeasures.getInstance();
    private List<FloatingActionButton> fabs = new ArrayList<>();
    private Notifiable notifiable;
    public boolean appeared = false;

    public FabMarkCoursesBucket(List<FloatingActionButton> fabs, Notifiable notifiable) {
        this.fabs = fabs;
        this.notifiable = notifiable;
        setCollapsable(this);
        setClickListeners();
    }

    public void appear() {
        if (!appeared) {
            fabs.get(0).startAnimation(translate(fabs.get(0), fabs.get(0).getX() + sm.dp(200), Direction.LEFT));
            appeared = true;
        }
    }

    private void showOtherFabs() {
        rises(fabs.get(1), sm.dp(75));
        rises(fabs.get(2), sm.dp(150));
    }

    public void vanish() {
        hideOtherFabs();
        appeared = false;
    }

    private void hideOtherFabs() {
        down(fabs.get(1), sm.dp(75));
        down(fabs.get(2), sm.dp(150));
    }

    @Override
    public void onAppearStart() {
        fabs.get(0).setVisibility(View.VISIBLE);
    }

    @Override
    public void onAppearEnd() {
        fabs.get(0).clearAnimation();
        showOtherFabs();
    }

    @Override
    public void onVanishStart() {
    }

    @Override
    public void onVanishEnd() {
        fabs.get(0).clearAnimation();
        fabs.get(0).setVisibility(View.GONE);
    }

    @Override
    public void onExpandStart(View view, float offset) {
        view.setVisibility(View.VISIBLE);
        view.setAlpha(1);
    }

    @Override
    public void onExpandEnd(View view, float offset) {
        view.clearAnimation();
        view.setY(view.getY() - offset);
    }

    @Override
    public void onCollapseStart(View view) {}

    @Override
    public void onCollapseEnd(View view) {
        view.clearAnimation();
        view.setY(fabs.get(0).getY());
        if (view.getId() == fabs.get(fabs.size() -1).getId()) {
            fabs.get(0).startAnimation(translate(fabs.get(0), fabs.get(0).getX() + sm.dp(100), Direction.RIGHT));
            notifiable.unkMarkButtonsIsGone();
        }
        view.setVisibility(View.GONE);
    }

    private void setClickListeners() {
        fabs.get(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notifiable.changeStatusOfCourses("Não cursada");
            }
        });

        fabs.get(1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notifiable.changeStatusOfCourses("Cursando");
            }
        });

        fabs.get(2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notifiable.changeStatusOfCourses("Cursada");
            }
        });
    }

    private void down(final View view, final int quantity) {
        view.startAnimation(translate(view, quantity, Direction.DOWN));
    }

    private void rises(final View view, final int quantity) {
        view.startAnimation(translate(view, quantity, Direction.UP));
    }
}
