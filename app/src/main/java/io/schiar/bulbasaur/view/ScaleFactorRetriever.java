package io.schiar.bulbasaur.view;

/**
 * Created by giovani on 26/09/15.
 */
public interface ScaleFactorRetriever {
    float getScalefactor();
}
