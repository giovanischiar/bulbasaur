package io.schiar.bulbasaur.view;

public interface Scalable {
    void updateScreen(float scaleFactor);
}
