package io.schiar.bulbasaur.view;

import android.view.View;

/**
 * Created by Giovani on 13/12/2015.
 */
public interface Collapsable {
    void onExpandStart(View view, float offset);
    void onExpandEnd(View view, float offset);
    void onCollapseStart(View view);
    void onCollapseEnd(View view);
}
