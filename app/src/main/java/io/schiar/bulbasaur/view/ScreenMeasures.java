package io.schiar.bulbasaur.view;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;

/**
 * Created by Giovani on 09/12/2015.
 */
public class ScreenMeasures {
    private Context context;
    private static ScreenMeasures ourInstance = new ScreenMeasures();
    public static ScreenMeasures getInstance() {
        return ourInstance;
    }
    public void setContext(Context context) {
        this.context = context;
    }
    private ScreenMeasures() {}

    public int color(int name) {
        return (int) context.getResources().getColor(name);
    }

    public int changeColorOpacity(int color, int opacity) {
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);
        return Color.argb(opacity, red, green, blue);
    }

    public int dimen(int id) {
        return (int) context.getResources().getDimension(id);
    }

    public int dp(int num) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, num, context.getResources().getDisplayMetrics());
    }

    public int sp(int num) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, num, context.getResources().getDisplayMetrics());
    }
}
