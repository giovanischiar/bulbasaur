package io.schiar.bulbasaur.persistence;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by User on 16/07/2015.
 */
public class DBCreator extends SQLiteOpenHelper {
    private static final String NAME_DATABASE = "database.db";
    private static final String COURSES = "courses";
    private static final String ID = "_id";
    private static final String ID1 = "_id1";
    private static final String NAME = "name";
    private static final String COD = "cod";
    private static final String SEMESTER = "semester";
    private static final String STATUS = "status";
    private static final int VERSION = 1;
    private SQLiteDatabase db;
    private static final String DEPENDENCIES = "dependencies";
    private static final String DEPENDS = "depends";
    private static final String DEPENDED = "depended";


    public DBCreator(Context context){
        super(context, NAME_DATABASE, null, VERSION);
    }

    public static String getID() {
        return ID;
    }

    public static String getNameDatabase() {
        return NAME_DATABASE;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        this.db = db;

        String sql1 = "CREATE TABLE " + COURSES + "("
                + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + NAME + " TEXT UNIQUE ON CONFLICT IGNORE, "
                + COD + " TEXT, "
                + SEMESTER + " INTEGER, "
                + STATUS + " TEXT "
                + ");";

        String sql2 = "CREATE TABLE " + DEPENDENCIES + "("
                + ID1 + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DEPENDS + " INTEGER, "
                + DEPENDED + " INTEGER, "
                + "FOREIGN KEY(" + DEPENDS + ") REFERENCES " + COURSES + "(" + ID + "), "
                + "FOREIGN KEY(" + DEPENDED + ") REFERENCES " + COURSES + "(" + ID + ")"
                +")";

        db.execSQL(sql1);
        db.execSQL(sql2);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public static String getCOURSES() {
        return COURSES;
    }

    public static String getNAME() {
        return NAME;
    }

    public static String getCOD() {
        return COD;
    }

    public static String getSEMESTER() {
        return SEMESTER;
    }

    public static String getSTATUS() {
        return STATUS;
    }

    public static String getDEPENDENCIES() {
        return DEPENDENCIES;
    }

    public static String getDEPENDS() {
        return DEPENDS;
    }

    public static String getDEPENDED() {
        return DEPENDED;
    }
}