package io.schiar.bulbasaur.persistence;

/**
 * Created by User on 09/08/2015.
 */
public enum RawQueries {
    AVAILABLE_COURSES("SELECT DISTINCT $.$, $.$, $.$, $.$, $.$ " +
                     "FROM $ $ JOIN $ ON $.$ = $ " +
                     "JOIN $ $ ON $.$ = $.$ " +
                     "WHERE $.$ = $ " +
                     "EXCEPT " +
                     "SELECT DISTINCT * " +
                     "FROM $ " +
                     "WHERE $ = $ " +
                     "EXCEPT " +
                     "SELECT DISTINCT $.$, $.$, $.$, $.$, $.$ " +
                     "FROM $ $ JOIN $ ON $.$ = $ " +
                              "JOIN $ $ ON $.$ = $.$ " +
                     "WHERE $.$ <> $ " +
                     "UNION " +
                     "SELECT * " +
                     "FROM $ " +
                     "WHERE $ NOT IN (SELECT $.$ " +
                                     "FROM $ $ JOIN $ ON $.$ = $.$ " +
                                              "JOIN $ $ ON $.$ = $.$) AND $ <> $"),

    DEPENDENCE_OF_COURSE("SELECT DISTINCT $.$, $.$, $.$, $.$, $.$ " +
                       "FROM $ $ JOIN $ ON $.$ = $ " +
                       "JOIN $ $ ON $.$ = $.$ " +
                       "WHERE $.$ = ?"),

    DERIVATES_OF_COURSE("SELECT DISTINCT $.$, $.$, $.$, $.$, $.$ " +
                      "FROM $ $ JOIN $ ON $.$ = $ " +
                      "JOIN $ $ ON $.$ = $.$ " +
                      "WHERE $.$ = ?"),

    ALL_DEPENDS_COURSES("SELECT * " +
                       "FROM $ " +
                       "WHERE $ IN (SELECT $ " +
                                   "FROM $ JOIN $ ON $ = $)"),

    UPDATE_STATUS_OF_COURSE("UPDATE $ " +
                         "SET $ = $ " +
                         "WHERE $ = ?"),

    STATUS_OF_COURSE("SELECT $ " +
                   "FROM $ " +
                   "WHERE $ = ?"),

    ALL_COURSES("SELECT * " +
                "FROM $"),

    BOUNDS("SELECT max($), max(row)" +
           "FROM (SELECT $, count($) AS row " +
                          "FROM $ GROUP BY $)");

    private String sql;

    RawQueries(String sql) {
        this.sql = sql;
    }

    public String getSQL() {
        return sql;
    }
}