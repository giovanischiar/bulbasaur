package io.schiar.bulbasaur.persistence;

/**
 * Created by User on 09/08/2015.
 */
public enum ArgsForQueries {
    ARGS_FOR_AVAILABLE_QUERY(new String[]{DBCreator.getDEPENDED(), DBCreator.getID(), DBCreator.getDEPENDED(), DBCreator.getNAME(), DBCreator.getDEPENDED(), DBCreator.getCOD(),
            DBCreator.getDEPENDED(),  DBCreator.getSEMESTER(), DBCreator.getDEPENDED(), DBCreator.getSTATUS(),
            DBCreator.getCOURSES(), DBCreator.getDEPENDS(), DBCreator.getDEPENDENCIES(), DBCreator.getDEPENDS(), DBCreator.getID(), DBCreator.getDEPENDS(),
            DBCreator.getCOURSES(), DBCreator.getDEPENDED(), DBCreator.getDEPENDED(), DBCreator.getID(), DBCreator.getDEPENDENCIES(), DBCreator.getDEPENDED(),
            DBCreator.getDEPENDS(), DBCreator.getSTATUS(), "\"cursada\"",
            DBCreator.getCOURSES(), DBCreator.getSTATUS(), "\"cursada\"",
            DBCreator.getDEPENDED(), DBCreator.getID(), DBCreator.getDEPENDED(), DBCreator.getNAME(), DBCreator.getDEPENDED(), DBCreator.getCOD(),
            DBCreator.getDEPENDED(),  DBCreator.getSEMESTER(), DBCreator.getDEPENDED(), DBCreator.getSTATUS(),
            DBCreator.getCOURSES(), DBCreator.getDEPENDS(), DBCreator.getDEPENDENCIES(), DBCreator.getDEPENDS(), DBCreator.getID(), DBCreator.getDEPENDS(),
            DBCreator.getCOURSES(), DBCreator.getDEPENDED(), DBCreator.getDEPENDED(), DBCreator.getID(), DBCreator.getDEPENDENCIES(), DBCreator.getDEPENDED(),
            DBCreator.getDEPENDS(), DBCreator.getSTATUS(), "\"cursada\"",
            DBCreator.getCOURSES(),
            DBCreator.getNAME(), DBCreator.getDEPENDED(), DBCreator.getNAME(),
            DBCreator.getCOURSES(), DBCreator.getDEPENDED(), DBCreator.getDEPENDENCIES(), DBCreator.getDEPENDED(), DBCreator.getID(), DBCreator.getDEPENDENCIES(),
            DBCreator.getDEPENDED(),
            DBCreator.getCOURSES(), DBCreator.getDEPENDS(), DBCreator.getDEPENDS(), DBCreator.getID(), DBCreator.getDEPENDENCIES(), DBCreator.getDEPENDS(), DBCreator.getSTATUS(),
            "\"cursada\""}),

    ARGS_FOR_DEPENDENCE_OF_COURSE(new String[]{DBCreator.getDEPENDS(), DBCreator.getID(), DBCreator.getDEPENDS(), DBCreator.getNAME(), DBCreator.getDEPENDS(), DBCreator.getSEMESTER(), DBCreator.getDEPENDS(), DBCreator.getSTATUS(),
                                           DBCreator.getCOURSES(), DBCreator.getDEPENDS(), DBCreator.getDEPENDENCIES(), DBCreator.getDEPENDS(), DBCreator.getID(), DBCreator.getDEPENDS(),
                                           DBCreator.getCOURSES(), DBCreator.getDEPENDED(), DBCreator.getDEPENDED(), DBCreator.getID(), DBCreator.getDEPENDENCIES(), DBCreator.getDEPENDED(),
                                           DBCreator.getNAME(), "?"}),

    ARGS_FOR_DERIVATIVES_OF_COURSE(new String[]{DBCreator.getDEPENDED(), DBCreator.getID(), DBCreator.getDEPENDED(), DBCreator.getNAME(), DBCreator.getDEPENDED(), DBCreator.getCOD(), DBCreator.getDEPENDED(),  DBCreator.getSEMESTER(), DBCreator.getDEPENDED(), DBCreator.getSTATUS(),
                                          DBCreator.getCOURSES(), DBCreator.getDEPENDS(), DBCreator.getDEPENDENCIES(), DBCreator.getDEPENDS(), DBCreator.getID(), DBCreator.getDEPENDS(),
                                          DBCreator.getCOURSES(), DBCreator.getDEPENDED(), DBCreator.getDEPENDED(), DBCreator.getID(), DBCreator.getDEPENDENCIES(), DBCreator.getDEPENDED(),
                                          DBCreator.getDEPENDS(),DBCreator.getNAME(), "?"}),

    ARGS_FOR_ALL_DEPENDED_COURSES(new String[]{DBCreator.getCOURSES(), DBCreator.getID(), DBCreator.getID(), DBCreator.getCOURSES(), DBCreator.getDEPENDENCIES(), DBCreator.getDEPENDS(), DBCreator.getID()}),

    ARGS_FOR_UPDATE_STATUS_OF_COURSE(new String[]{DBCreator.getCOURSES(), DBCreator.getSTATUS(), "?", DBCreator.getNAME(), "?"}),

    ARGS_FOR_STATUS_OF_COURSE(new String[]{DBCreator.getSTATUS(), DBCreator.getCOURSES(), DBCreator.getNAME(), "?"}),

    ARGS_FOR_ALL_COURSES(new String[]{DBCreator.getCOURSES()}),

    ARGS_FOR_BOUNDS(new String[]{DBCreator.getSEMESTER(), DBCreator.getSEMESTER(), DBCreator.getSEMESTER(), DBCreator.getCOURSES(), DBCreator.getSEMESTER()});

    private String[] args;
    ArgsForQueries(String[] args) {
        this.args = args;
    }

    public String[] getArgs() {
        return args;
    }
}
