package io.schiar.bulbasaur.persistence;

/**
 * Created by User on 09/08/2015.
 */
public enum Queries {
    AVAILABLE_COURSES(RawQueries.AVAILABLE_COURSES.getSQL(), ArgsForQueries.ARGS_FOR_AVAILABLE_QUERY.getArgs()),
    DEPENDENCE_OF_COURSE(RawQueries.DEPENDENCE_OF_COURSE.getSQL(), ArgsForQueries.ARGS_FOR_DEPENDENCE_OF_COURSE.getArgs()),
    DERIVATIVES_OF_COURSE(RawQueries.DERIVATES_OF_COURSE.getSQL(), ArgsForQueries.ARGS_FOR_DERIVATIVES_OF_COURSE.getArgs()),
    ALL_DEPENDS_COURSES(RawQueries.ALL_DEPENDS_COURSES.getSQL(), ArgsForQueries.ARGS_FOR_ALL_DEPENDED_COURSES.getArgs()),
    UPDATE_STATUS_OF_COURSE(RawQueries.UPDATE_STATUS_OF_COURSE.getSQL(), ArgsForQueries.ARGS_FOR_UPDATE_STATUS_OF_COURSE.getArgs()),
    STATUS_OF_COURSE(RawQueries.STATUS_OF_COURSE.getSQL(), ArgsForQueries.ARGS_FOR_STATUS_OF_COURSE.getArgs()),
    ALL_COURSES(RawQueries.ALL_COURSES.getSQL(), ArgsForQueries.ARGS_FOR_ALL_COURSES.getArgs()),
    BOUNDS(RawQueries.BOUNDS.getSQL(), ArgsForQueries.ARGS_FOR_BOUNDS.getArgs());

    private String sql;
    Queries(String sql, String[] args) {
        this.sql = sql;
        for(String arg : args) {
            this.sql = this.sql.replaceFirst("\\$", arg);
        }
    }

    public String getSql() {
        return sql;
    }
}
