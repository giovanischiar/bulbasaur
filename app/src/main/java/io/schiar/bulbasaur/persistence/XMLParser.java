package io.schiar.bulbasaur.persistence;

import android.util.Log;

import io.schiar.bulbasaur.model.Course;
import io.schiar.bulbasaur.model.Curriculum;
import io.schiar.bulbasaur.model.Dependency;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 05/08/2015.
 */
public class XMLParser {

    List<Course> courses;
    Curriculum curriculum;
    private Course course;
    private List<Course> dependeds;
    private Dependency dependency;
    private String text;

    public XMLParser() {
        courses = new ArrayList<>();
    }

    public List<Course> getCourses() {
        return courses;
    }

    public Curriculum parse(InputStream is) {

        XmlPullParserFactory factory = null;
        XmlPullParser parser = null;

        try {
            factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);

            parser = factory.newPullParser();
            parser.setInput(is, null);

            int eventType = parser.getEventType();
            while(eventType != XmlPullParser.END_DOCUMENT) {
                String tagname = parser.getName();
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if(tagname.equalsIgnoreCase("course")) {
                            course = new Course();
                        } else if(tagname.equalsIgnoreCase("curriculum")) {
                            curriculum = new Curriculum();
                        } else if(tagname.equalsIgnoreCase("dependency")) {
                            dependency = new Dependency();
                        } else if(tagname.equalsIgnoreCase("dependeds")) {
                            dependeds = new ArrayList<>();
                        }
                        break;

                    case XmlPullParser.TEXT:
                        text = parser.getText();
                        break;

                    case XmlPullParser.END_TAG:
                        if(tagname.equalsIgnoreCase("course")) {
                            courses.add(course);
                        } else if(tagname.equalsIgnoreCase("namecurriculum")) {
                            curriculum.setName(text);
                        } else if(tagname.equalsIgnoreCase("fullname")) {
                            course.setFullName(text);
                        } else if(tagname.equalsIgnoreCase("name")) {
                            course.setName(text);
                        } else if(tagname.equalsIgnoreCase("code")) {
                            course.setCode(text);
                        } else if(tagname.equalsIgnoreCase("semester")) {
                            course.setPhase(Integer.parseInt(text));
                        } else if(tagname.equalsIgnoreCase("courses")) {
                            curriculum.setCourses(courses);
                        } else if(tagname.equalsIgnoreCase("depends")) {
                            dependency.setDepends(gimmeCourseByName(text));
                        }else if(tagname.equalsIgnoreCase("depended")) {
                            dependeds.add(gimmeCourseByName(text));
                        }else if(tagname.equalsIgnoreCase("dependency")) {
                            dependency.setDepended(dependeds.toArray(new Course[dependeds.size()]));
                            curriculum.addDependency(dependency);
                        }
                        break;
                    default:
                        break;
                }
                eventType = parser.next();
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return curriculum;
    }

    public Course gimmeCourseByName(String name) {
        for(Course course : courses) {
            if (course.getName().equals(name)) {
                return course;
            }
        }
        Log.wtf("XMLPARSERERROR", "course: " + name + " not exists");
        return null;
    }
}