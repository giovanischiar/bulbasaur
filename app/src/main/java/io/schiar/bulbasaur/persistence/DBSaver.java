package io.schiar.bulbasaur.persistence;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

/**
 * Created by giovani on 27/09/15.
 */
public class DBSaver {
    public void copyDBFile(File dbFile) {
        File dbCopied = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/bulbassaur_" + System.currentTimeMillis() + ".db");
        Log.wtf("banco de dados", dbCopied.getAbsolutePath());
        try {
            if(dbCopied.createNewFile()) {
                File currentDB = dbFile;
                FileChannel from = null;
                FileChannel to = null;
                try {
                    from = new FileInputStream(currentDB).getChannel();
                    to = new FileOutputStream(dbCopied).getChannel();
                    from.transferTo(0, from.size(), to);
                } finally {
                    if(from != null) {
                        from.close();
                    }
                    if(to != null) {
                        to.close();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}